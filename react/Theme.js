import React, { Component } from 'react'

export default (props) => {

	return (
		<div>
			<section id="header">
				<header>
					<span className="image avatar"><img src="/images/avatar.jpg" alt="" /></span>
					<h1 id="logo"><a href="#">Willis Corto</a></h1>
					<p>I got reprogrammed by a rogue AI<br />
					and now Im totally cray</p>
				</header>
				<nav id="nav">
					<ul>
						<li><a href="#one" className="active">About</a></li>
						<li><a href="#two">Things I Can Do</a></li>
						<li><a href="#three">A Few Accomplishments</a></li>
						<li><a href="#four">Contact</a></li>
					</ul>
				</nav>
				<footer>
					<ul className="icons">
						<li><a href="#" className="icon fa-twitter"><span className="label">Twitter</span></a></li>
						<li><a href="#" className="icon fa-facebook"><span className="label">Facebook</span></a></li>
						<li><a href="#" className="icon fa-instagram"><span className="label">Instagram</span></a></li>
						<li><a href="#" className="icon fa-github"><span className="label">Github</span></a></li>
						<li><a href="#" className="icon fa-envelope"><span className="label">Email</span></a></li>
					</ul>
				</footer>
			</section>

			<div id="wrapper">
				<div id="main">
					<section id="one">
						<div className="container">
							<header className="major">
								<h2>Read Only</h2>
								<p>Just an incredibly simple responsive site<br />
								template freebie by <a href="http://html5up.net">HTML5 UP</a>.</p>
							</header>
							<p>Faucibus sed lobortis aliquam lorem blandit. Lorem eu nunc metus col. Commodo id in arcu ante lorem ipsum sed accumsan erat praesent faucibus commodo ac mi lacus. Adipiscing mi ac commodo. Vis aliquet tortor ultricies non ante erat nunc integer eu ante ornare amet commetus vestibulum blandit integer in curae ac faucibus integer non. Adipiscing cubilia elementum.</p>
						</div>
					</section>

					<section id="two">
						<div className="container">
							<h3>Things I Can Do</h3>
							<p>Integer eu ante ornare amet commetus vestibulum blandit integer in curae ac faucibus integer non. Adipiscing cubilia elementum integer lorem ipsum dolor sit amet.</p>
							<ul className="feature-icons">
								<li className="fa-code">Write all the code</li>
								<li className="fa-cubes">Stack small boxes</li>
								<li className="fa-book">Read books and stuff</li>
								<li className="fa-coffee">Drink much coffee</li>
								<li className="fa-bolt">Lightning bolt</li>
								<li className="fa-users">Shadow clone technique</li>
							</ul>
						</div>
					</section>

					<section id="three">
						<div className="container">
							<h3>A Few Accomplishments</h3>
							<p>Integer eu ante ornare amet commetus vestibulum blandit integer in curae ac faucibus integer non. Adipiscing cubilia elementum integer. Integer eu ante ornare amet commetus.</p>
							<div className="features">
								<article>
									<a href="#" className="image"><img src="/images/pic01.jpg" alt="" /></a>
									<div className="inner">
										<h4>Possibly broke spacetime</h4>
										<p>Integer eu ante ornare amet commetus vestibulum blandit integer in curae ac faucibus integer adipiscing ornare amet.</p>
									</div>
								</article>
								<article>
									<a href="#" className="image"><img src="/images/pic02.jpg" alt="" /></a>
									<div className="inner">
										<h4>Terraformed a small moon</h4>
										<p>Integer eu ante ornare amet commetus vestibulum blandit integer in curae ac faucibus integer adipiscing ornare amet.</p>
									</div>
								</article>
								<article>
									<a href="#" className="image"><img src="/images/pic03.jpg" alt="" /></a>
									<div className="inner">
										<h4>Snapped dark matter in the wild</h4>
										<p>Integer eu ante ornare amet commetus vestibulum blandit integer in curae ac faucibus integer adipiscing ornare amet.</p>
									</div>
								</article>
							</div>
						</div>
					</section>

					<section id="four">
						<div className="container">
							<h3>Contact Me</h3>
							<p>Integer eu ante ornare amet commetus vestibulum blandit integer in curae ac faucibus integer non. Adipiscing cubilia elementum integer. Integer eu ante ornare amet commetus.</p>
							<form method="post" action="#">
								<div className="row uniform">
									<div className="6u 12u(xsmall)"><input type="text" name="name" id="name" placeholder="Name" /></div>
									<div className="6u 12u(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
								</div>
								<div className="row uniform">
									<div className="12u"><input type="text" name="subject" id="subject" placeholder="Subject" /></div>
								</div>
								<div className="row uniform">
									<div className="12u"><textarea name="message" id="message" placeholder="Message" rows="6"></textarea></div>
								</div>
								<div className="row uniform">
									<div className="12u">
										<ul className="actions">
											<li><input type="submit" className="special" value="Send Message" /></li>
											<li><input type="reset" value="Reset Form" /></li>
										</ul>
									</div>
								</div>
							</form>
						</div>
					</section>

					<section id="five">
						<div className="container">
							<h3>Elements</h3>

							<section>
								<h4>Text</h4>
								<p>This is <b>bold</b> and this is <strong>strong</strong>. This is <i>italic</i> and this is <em>emphasized</em>.
								This is <sup>superscript</sup> text and this is <sub>subscript</sub> text.
								This is <u>underlined</u>. Finally, <a href="#">this is a link</a>.</p>
								<hr />
								<header>
									<h4>Heading with a Subtitle</h4>
									<p>Lorem ipsum dolor sit amet nullam id egestas urna aliquam</p>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
								<header>
									<h5>Heading with a Subtitle</h5>
									<p>Lorem ipsum dolor sit amet nullam id egestas urna aliquam</p>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
								<hr />
								<h2>Heading Level 2</h2>
								<h3>Heading Level 3</h3>
								<h4>Heading Level 4</h4>
								<h5>Heading Level 5</h5>
								<h6>Heading Level 6</h6>
								<hr />
								<h5>Blockquote</h5>
								<blockquote>Fringilla nisl. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan faucibus. Vestibulum ante ipsum primis in faucibus lorem ipsum dolor sit amet nullam adipiscing eu felis.</blockquote>
							</section>

							<section>
								<h4>Lists</h4>
								<div className="row">
									<div className="6u 12u(xsmall)">
										<h5>Unordered</h5>
										<ul>
											<li>Dolor pulvinar etiam magna etiam.</li>
											<li>Sagittis adipiscing lorem eleifend.</li>
											<li>Felis enim feugiat dolore viverra.</li>
										</ul>
										<h5>Alternate</h5>
										<ul className="alt">
											<li>Dolor pulvinar etiam magna etiam.</li>
											<li>Sagittis adipiscing lorem eleifend.</li>
											<li>Felis enim feugiat dolore viverra.</li>
										</ul>
									</div>
									<div className="6u 12u(xsmall)">
										<h5>Ordered</h5>
										<ol>
											<li>Dolor pulvinar etiam magna etiam.</li>
											<li>Etiam vel felis at lorem sed viverra.</li>
											<li>Felis enim feugiat dolore viverra.</li>
											<li>Dolor pulvinar etiam magna etiam.</li>
											<li>Etiam vel felis at lorem sed viverra.</li>
											<li>Felis enim feugiat dolore viverra.</li>
										</ol>
										<h5>Icons</h5>
										<ul className="icons">
											<li><a href="#" className="icon fa-twitter"><span className="label">Twitter</span></a></li>
											<li><a href="#" className="icon fa-facebook"><span className="label">Facebook</span></a></li>
											<li><a href="#" className="icon fa-instagram"><span className="label">Instagram</span></a></li>
											<li><a href="#" className="icon fa-github"><span className="label">Github</span></a></li>
											<li><a href="#" className="icon fa-dribbble"><span className="label">Dribbble</span></a></li>
											<li><a href="#" className="icon fa-tumblr"><span className="label">Tumblr</span></a></li>
										</ul>
									</div>
								</div>
								<h5>Actions</h5>
								<ul className="actions">
									<li><a href="#" className="button special">Default</a></li>
									<li><a href="#" className="button">Default</a></li>
									<li><a href="#" className="button alt">Default</a></li>
								</ul>
								<ul className="actions small">
									<li><a href="#" className="button special small">Small</a></li>
									<li><a href="#" className="button small">Small</a></li>
									<li><a href="#" className="button alt small">Small</a></li>
								</ul>
								<div className="row">
									<div className="3u 6u(medium) 12u$(xsmall)">
										<ul className="actions vertical">
											<li><a href="#" className="button special">Default</a></li>
											<li><a href="#" className="button">Default</a></li>
											<li><a href="#" className="button alt">Default</a></li>
										</ul>
									</div>
									<div className="3u 6u$(medium) 12u$(xsmall)">
										<ul className="actions vertical small">
											<li><a href="#" className="button special small">Small</a></li>
											<li><a href="#" className="button small">Small</a></li>
											<li><a href="#" className="button alt small">Small</a></li>
										</ul>
									</div>
									<div className="3u 6u(medium) 12u$(xsmall)">
										<ul className="actions vertical">
											<li><a href="#" className="button special fit">Default</a></li>
											<li><a href="#" className="button fit">Default</a></li>
											<li><a href="#" className="button alt fit">Default</a></li>
										</ul>
									</div>
									<div className="3u 6u$(medium) 12u$(xsmall)">
										<ul className="actions vertical small">
											<li><a href="#" className="button special small fit">Small</a></li>
											<li><a href="#" className="button small fit">Small</a></li>
											<li><a href="#" className="button alt small fit">Small</a></li>
										</ul>
									</div>
								</div>
							</section>

							<section>
								<h4>Table</h4>
								<h5>Default</h5>
								<div className="table-wrapper">
									<table>
										<thead>
											<tr>
												<th>Name</th>
												<th>Description</th>
												<th>Price</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Item One</td>
												<td>Ante turpis integer aliquet porttitor.</td>
												<td>29.99</td>
											</tr>
											<tr>
												<td>Item Two</td>
												<td>Vis ac commodo adipiscing arcu aliquet.</td>
												<td>19.99</td>
											</tr>
											<tr>
												<td>Item Three</td>
												<td> Morbi faucibus arcu accumsan lorem.</td>
												<td>29.99</td>
											</tr>
											<tr>
												<td>Item Four</td>
												<td>Vitae integer tempus condimentum.</td>
												<td>19.99</td>
											</tr>
											<tr>
												<td>Item Five</td>
												<td>Ante turpis integer aliquet porttitor.</td>
												<td>29.99</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="2"></td>
												<td>100.00</td>
											</tr>
										</tfoot>
									</table>
								</div>

								<h5>Alternate</h5>
								<div className="table-wrapper">
									<table className="alt">
										<thead>
											<tr>
												<th>Name</th>
												<th>Description</th>
												<th>Price</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Item One</td>
												<td>Ante turpis integer aliquet porttitor.</td>
												<td>29.99</td>
											</tr>
											<tr>
												<td>Item Two</td>
												<td>Vis ac commodo adipiscing arcu aliquet.</td>
												<td>19.99</td>
											</tr>
											<tr>
												<td>Item Three</td>
												<td> Morbi faucibus arcu accumsan lorem.</td>
												<td>29.99</td>
											</tr>
											<tr>
												<td>Item Four</td>
												<td>Vitae integer tempus condimentum.</td>
												<td>19.99</td>
											</tr>
											<tr>
												<td>Item Five</td>
												<td>Ante turpis integer aliquet porttitor.</td>
												<td>29.99</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="2"></td>
												<td>100.00</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</section>

							<section>
								<h4>Buttons</h4>
								<ul className="actions">
									<li><a href="#" className="button special">Special</a></li>
									<li><a href="#" className="button">Default</a></li>
									<li><a href="#" className="button alt">Alternate</a></li>
								</ul>
								<ul className="actions">
									<li><a href="#" className="button special big">Big</a></li>
									<li><a href="#" className="button">Default</a></li>
									<li><a href="#" className="button alt small">Small</a></li>
								</ul>
								<ul className="actions fit">
									<li><a href="#" className="button special fit">Fit</a></li>
									<li><a href="#" className="button fit">Fit</a></li>
									<li><a href="#" className="button alt fit">Fit</a></li>
								</ul>
								<ul className="actions fit small">
									<li><a href="#" className="button special fit small">Fit + Small</a></li>
									<li><a href="#" className="button fit small">Fit + Small</a></li>
									<li><a href="#" className="button alt fit small">Fit + Small</a></li>
								</ul>
								<ul className="actions">
									<li><a href="#" className="button special icon fa-download">Icon</a></li>
									<li><a href="#" className="button icon fa-download">Icon</a></li>
									<li><a href="#" className="button alt icon fa-check">Icon</a></li>
								</ul>
								<ul className="actions">
									<li><span className="button special disabled">Special</span></li>
									<li><span className="button disabled">Default</span></li>
									<li><span className="button alt disabled">Alternate</span></li>
								</ul>
							</section>

							<section>
								<h4>Form</h4>
								<form method="post" action="#">
									<div className="row uniform">
										<div className="6u 12u(xsmall)">
											<input type="text" name="demo-name" id="demo-name" value="" placeholder="Name" />
										</div>
										<div className="6u 12u(xsmall)">
											<input type="email" name="demo-email" id="demo-email" value="" placeholder="Email" />
										</div>
									</div>
									<div className="row uniform">
										<div className="12u">
											<div className="select-wrapper">
												<select name="demo-category" id="demo-category">
													<option value="">- Category -</option>
													<option value="1">Manufacturing</option>
													<option value="1">Shipping</option>
													<option value="1">Administration</option>
													<option value="1">Human Resources</option>
												</select>
											</div>
										</div>
									</div>
									<div className="row uniform">
										<div className="4u 12u(medium)">
											<input type="radio" id="demo-priority-low" name="demo-priority" checked />
											<label for="demo-priority-low">Low Priority</label>
										</div>
										<div className="4u 12u(medium)">
											<input type="radio" id="demo-priority-normal" name="demo-priority" />
											<label for="demo-priority-normal">Normal Priority</label>
										</div>
										<div className="4u 12u(medium)">
											<input type="radio" id="demo-priority-high" name="demo-priority" />
											<label for="demo-priority-high">High Priority</label>
										</div>
									</div>
									<div className="row uniform">
										<div className="6u 12u(medium)">
											<input type="checkbox" id="demo-copy" name="demo-copy" />
											<label for="demo-copy">Email me a copy of this message</label>
										</div>
										<div className="6u 12u(medium)">
											<input type="checkbox" id="demo-human" name="demo-human" checked />
											<label for="demo-human">I am a human and not a robot</label>
										</div>
									</div>
									<div className="row uniform">
										<div className="12u">
											<textarea name="demo-message" id="demo-message" placeholder="Enter your message" rows="6"></textarea>
										</div>
									</div>
									<div className="row uniform">
										<div className="12u">
											<ul className="actions">
												<li><input type="submit" value="Send Message" /></li>
												<li><input type="reset" value="Reset" className="alt" /></li>
											</ul>
										</div>
									</div>
								</form>
							</section>

							<section>
								<h4>Image</h4>
								<h5>Fit</h5>
								<span className="image fit"><img src="/images/banner.jpg" alt="" /></span>
								<div className="box alt">
									<div className="row 50% uniform">
										<div className="4u"><span className="image fit"><img src="/images/pic01.jpg" alt="" /></span></div>
										<div className="4u"><span className="image fit"><img src="/images/pic02.jpg" alt="" /></span></div>
										<div className="4u"><span className="image fit"><img src="/images/pic03.jpg" alt="" /></span></div>
									</div>
									<div className="row 50% uniform">
										<div className="4u"><span className="image fit"><img src="/images/pic02.jpg" alt="" /></span></div>
										<div className="4u"><span className="image fit"><img src="/images/pic03.jpg" alt="" /></span></div>
										<div className="4u"><span className="image fit"><img src="/images/pic01.jpg" alt="" /></span></div>
									</div>
									<div className="row 50% uniform">
										<div className="4u"><span className="image fit"><img src="/images/pic03.jpg" alt="" /></span></div>
										<div className="4u"><span className="image fit"><img src="/images/pic01.jpg" alt="" /></span></div>
										<div className="4u"><span className="image fit"><img src="/images/pic02.jpg" alt="" /></span></div>
									</div>
								</div>
								<h5>Left &amp; Right</h5>
								<p><span className="image left"><img src="/images/avatar.jpg" alt="" /></span>Fringilla nisl. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent.</p>
								<p><span className="image right"><img src="/images/avatar.jpg" alt="" /></span>Fringilla nisl. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent.</p>
							</section>

						</div>
					</section>
				</div>

				<section id="footer">
					<div className="container">
						<ul className="copyright">
							<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</div>
				</section>
			</div>
		</div>
	)

}